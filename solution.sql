//1
SELECT customerName from customers WHERE country = "Philippines";

//2
SELECT contactLastName, contactFirstName from customers WHERE customerName = "La Rochelle Gifts";

//3
SELECT productName, MSRP from products WHERE productName = "The Titanic";

//4
SELECT firstName, lastName from employees WHERE email = "jfirrelli@classicmodelcars.com";

//5
SELECT customerName from customers WHERE state IS NULL;

//6
SELECT firstName, lastName, email from employees WHERE lastName = "Patterson" AND firstName = "Steve";

//7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

//8
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

//9
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

//10
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

//11
SELECT DISTINCT country FROM customers;

//12
SELECT DISTINCT status FROM orders;

//13
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

//14
SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

//15
SELECT customers.customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.lastName = "Thompson"
AND employees.firstName = "Leslie";

//16
SELECT products.productName, customers.customerName FROM customers 
	LEFT OUTER JOIN orders ON orders.customerNumber = customers.customerNumber
	LEFT OUTER JOIN orderdetails on orderdetails.orderNumber = orders.orderNumber
	LEFT OUTER JOIN products on orderdetails.productcode = products.productcode
	WHERE customers.customerName = "Baane Mini Imports";

//17
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees, customers, offices WHERE employees.officeCode = offices.officeCode AND offices.country = customers.country;

//18
SELECT employees.lastName, employees.firstName FROM employees WHERE reportsTo = "1143";

//19
SELECT productName, MAX(MSRP) FROM products;


//20
SELECT COUNT(*) FROM customers WHERE country = "UK";

//21
SELECT productLine, COUNT(*) FROM products
	GROUP BY productLine;

//22
SELECT salesRepEmployeeNumber, COUNT(*) FROM customers GROUP BY salesRepEmployeeNumber;

//23
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;